.. イエス・キリスト教 documentation master file, created by
   sphinx-quickstart on Thu Jul  4 10:46:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. meta::
   :description: Create functional and beautiful websites for your documentation with Sphinx and the Awesome Sphinx Theme.
   :twitter:description: Create functional and beautiful websites for your documentation with Sphinx and the Awesome Sphinx Theme.


Welcome to イエス・キリスト教's documentation!
==============================================

.. rst-class:: lead

   Create functional and beautiful websites for your documentation with Sphinx.

----

Get started
-----------

.. note::

   You're viewing the documentation for version |current| of the |product|,
   which is a **beta** release.
   That's why you have to add ``--pre`` to the installation command.
   To read the documentation for the latest stable release, see :doc:`v4:index` (version 4).

#. Install the theme:

   .. literalinclude:: how-to/install/includes/install.sh
      :language: sh

   .. seealso::

      :doc:`how-to/install/index`

#. Add the theme to your Sphinx configuration:

   .. literalinclude:: how-to/add/includes/configure.inc
      :language: python
      :caption: |conf|

   .. seealso::

      :doc:`how-to/add/index`

#. Build your documentation.

   .. seealso::

      `Get started with Sphinx <https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html>`_

Upgrade
-------

If you want to upgrade to version |current| of the theme, see :ref:`sec:upgrade-to-5.0`.
If you don't want to upgrade, you can read the `documentation for version 4 <https://v4--sphinxawesome-theme.netlify.app/>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

  .. toctree::
    :maxdepth: 1
    :titlesonly:
    :caption: Issues
    :name: multiple-issues:

    Windows <devops/issues/windows>
    Linux <devops/issues/linux>


  .. toctree::
    :maxdepth: 1
    :titlesonly:
    :caption: Security
    :name: multiple-security:

    Syn Flood Attack <devops/security/syn_flood_attack>


  .. toctree::
    :maxdepth: 1
    :titlesonly:
    :caption: Projects
    :name: sec-projects:

    Indicadores GST <devops/projects/gst/Operaciones/Indicadores>
    Disponiilidad de Unidades GST <devops/projects/gst/Operaciones/DisponibilidadDeUnidades>
    Web Service Copiloto <devops/projects/gst/Operaciones/Copiloto>
    Backups <devops/projects/gst/Server/backups>
    Addenum <devops/projects/gst/Operaciones/Addenum>
    CFDI validations <devops/projects/gst/Finanzas/CfdiValidation>
    Web Service 8w <devops/projects/8w/Operaciones/InterfacesWs>
    Raw Material <devops/projects/UES/RawMaterialsWarehouse>
    devops/projects/Sie/speech
    VPN <devops/projects/gst/VirtualPrivateNetwork/VpnImplementation>
    PYCMXML <devops/projects/UES/pycmxml/README.md>

  .. toctree::
    :maxdepth: 2
    :titlesonly:
    :caption: Roadmap

    Development Roadmap <devops/Roadmap/2023/WorkRoadmap>


 Indices and tables
 ==================
 
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`


License
-------

The project is licensed under the BSD license.

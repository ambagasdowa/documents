Documentacion Api Gal
=======================

.. .. contents:: Indice

Interfaz Viajes [WS]
-------------------------------------------
sa:Logistica95:10.44.45.18

  La Interfaz de Viajes [ApiGalViajes] Admite los metodos necesarios para crear un viaje en el sistemas ZAM

  metodos:

  {Diagrama}


  View

    Se Utiliza para obtener informacion de un viaje mediante un id
      view/x
      donde x = id del registro

  Add [POST]
    El Metodo Agrega un registro con la informacion necesaria para crear un viaje en el ERP de ZAM
  Parametros:
    Anticipo:
      Headers:

        Accept:application/json
        Content-Type:application/json

      Body:
        {
        	"sie_ctrl":2
        	,"api_ctrl":10013
        	,"anticipo_fecha_anticipo":"2019-09-15T22:10:15"
        	,"anticipo_monto_anticipo":2023.30
        	,"anticipo_monto_anticipo_iva":125.20
        	,"anticipo_fecha_ingreso":"2019-09-10T22:10:15"
        	,"anticipo_observaciones":"AntInsertWs03"
        	,"api_id_personal":92
        }

        Viaje:
          Headers:

            Accept:application/json
            Content-Type:application/json

          Body:
          {
          		 "sie_ctrl":1
          		,"api_ctrl":10013
          		,"api_id_ruta":51
          		,"asignacion_num_asignacion":1
          		,"asignacion_id_configuracionviaje":1
          		,"api_id_personal":92
          		,"api_id_unidad":"G-169"
          		,"api_id_remolque1":"RV3560"
          		,"api_id_remolque2":"RV4456"
          		,"api_id_dolly":1
          		,"api_no_viaje":117383
          		,"api_fecha_ingreso":"2019-09-15T22:10:15"
          		,"api_f_prog_ini_viaje":"2019-09-15T22:10:15"
          		,"api_f_prog_fin_viaje":"2019-09-15T22:10:15"
          		,"asignacion_id_seguimiento":1
          		,"asignacion_kms_ruta":1
          		,"api_id_remitente":1
          		,"api_id_destinatario":1
          		,"viaje_kms_camion_vacio":1
          		,"viaje_kms_camion_lleno":1
          		,"viaje_kms_operador":1
          		,"api_id_origen":1
          		,"api_id_destino":1
          		,"pedido_id_producto":1
          		,"pedido_f_pedido":"2019-09-15T22:10:15"
          		,"pedido_id_cliente":0
          	}

            CartaPorte:
              Headers:

                Accept:application/json
                Content-Type:application/json

              Body:
              {
              	 "sie_ctrl":3
              	,"api_ctrl":10013
              	,"pedido_id_cliente":0
              	,"api_id_personal":92
              	,"api_id_remitente":1
              	,"api_id_destinatario":1
              	,"asignacion_kms_ruta":300
              	,"api_id_origen":149
              	,"api_id_destino":105
              	,"guia_num_guia":"0"
              	,"guia_fecha_guia":"2019-09-15T22:10:15"
              	,"guia_flete":1.00
              	,"guia_otro":1.00
              	,"guia_subtotal":1.00
              	,"guia_iva_guia":1.00
              	,"guia_monto_retencion":1.00
              	,"guia_id_serieguia":1
              	,"guia_id_iva":1
              	,"guia_id_retencion":1
              	,"guia_id_metodo_pago":1
              	,"guia_no_cuenta":0.00
              	,"renglon_id_producto":1
              	,"renglon_id_fraccion":1
              	,"otro_monto_iva_otro":1.00
              	,"otro_desc_otro":"Nombre"
              	,"otro_id_otro":1
              }


  Edit

  Delete

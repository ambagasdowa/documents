***********************************
Backups Process of databases files 
***********************************

.. .. contents:: Indice



Estructura
==========

:Servidores:
  - Integra_db      [192.168.20.235]
  - Integra_storage [192.168.20.216]
  - Integra_nas     [192.168.20.168]

:Command:

.. literalinclude:: ../../../../_static/source/Compress.sh
   :language: bash
   :linenos:


UML Diagram
===========

  .. graphviz:: ../../../../_static/dot/backup.dot

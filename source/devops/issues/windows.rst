***************************
Issues with Windows Systems
***************************

.. .. contents:: Indice



RDP Section
===========

HOW TO TROUBLESHOOT “THE TERMINAL SERVER SECURITY LAYER DETECTED AN ERROR IN THE PROTOCOL STREAM AND HAS DISCONNECTED THE CLIENT. CLIENT IP:” AND “THE RDP PROTOCOL COMPONENT X.224 DETECTED AN ERROR IN THE PROTOCOL STREAM AND HAS DISCONNECTED THE CLIENT”ERROR MESSAGES
11/06/2017 itsolutiondesign1 Comment

PROBLEM DESCRIPTION :
---------------------

You may experience problems if you try to connect to a Windows Server 2008 R2 via RDP. This can also occur in a XenDesktop 7 site with a Windows Server 2008 R2 broker server.

SYMPTOMS :
----------
– RDP Session may freeze.
– Black screen inside RDP window.
– Slow connection.
– You may also be disconnected.
-ICA Sessions may be disconnected without notice

ERROR MESSAGES :
----------------

- Log Name: System
- Source: TermDD
- Date: 28.02.2012 08:49:40
- Event ID: 56
- Task Category: None
- Level: Error
- Keywords: Classic
- User: N/A
- Computer: XXXXX
Description:
  The Terminal Server security layer detected an error in the protocol stream and has disconnected the client. Client IP: xx.xx.xx.xx
- Log Name: System
- Source: TermDD
- Date: 25.02.2012 23:00:59
- Event ID: 50
- Task Category: None
- Level: Error
- Keywords: Classic
- User: N/A
- Computer: XXXXX
Description:
  The RDP protocol component X.224 detected an error in the protocol stream and has disconnected the client.

SOLUTION :
----------

The following actions solved the problem in our case.

1) Configure TCP Chimney Offload in the operating system
• To disable TCP Chimney Offload, follow these steps:
a. Use administrative credentials to open a command prompt.
b. At the command prompt, type the following command, and then press ENTER:
netsh int tcp set global chimney=disabled
2) Disable RSS in Windows Server 2008 R2
• To disable RSS, follow these steps:
1. Use administrative credentials to open a command prompt.
2. At the command prompt, type the following command, and then press ENTER:
netsh int tcp set global rss=disabled
• To determine the current status of RSS, follow these steps:
a. Use administrative credentials to open a command prompt.
b. At the command prompt, type the following command, and then press ENTER:
netsh int tcp show global


3) Disable NetDMA in Windows Server 2008 R2
• To disable NetDMA, follow these steps:
1. Click Start, click Run, type regedit, and then click OK.
2. Locate the following registry subkey, and then click it:

.. code-block:: bat

   HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters

3. Double-click the EnableTCPA registry entry.
Note If this registry entry does not exist, right-click Parameters, point to New, click DWORD Value, type EnableTCPA, and then press ENTER.
4. To enable NetDMA, type 1 in the Value data box, and then click OK.
5. To disable NetDMA, type 0 in the Value data box, and then click OK.

Information about the TCP Chimney Offload, Receive Side Scaling, and Network Direct Memory Access features in Windows Server 2008
Source : http://support.microsoft.com/kb/951037/en-us

If none of the above does the trick, you can change the Security Layer at the RDP stack and set it to RDP Security Layer from Negotiate

You can change it by going to Remote Desktop Host Configuration – General

.. image:: ../../_static/windows/capture10.png


If the above doesn’t solve the issue for you, the NW card could have gone faulty

..
  .. graphviz:: ../../../../_static/dot/ws.dot
..

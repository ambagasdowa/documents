******************
Interfaz Unigis-8W
******************

.. .. contents:: Indice


Estructura del Sistema
======================

:Components of the system:
   - Cakephp Version 3.7.9
   - vendor xmlSoap 1.0.0
   - debian 9.x
   - nginx


Modules
=======
  - Pedidos


Api Flow
========

  .. graphviz:: ../../../../_static/dot/flujo.dot




  Structure of Unigis
  -------------------

  .. image:: ../../../../_static/img/eightw/wsunigis.png

  - nginx configuration

  .. code-block:: conf

     # Default server configuration
     server {
             listen 80 default_server;
             listen [::]:80 default_server;

             root /var/www/html/gst/webroot;
             autoindex off;
             # Add index.php to the list if you are using PHP
             index index.php index.html index.htm index.nginx-debian.html;

             server_name _;

             location / {
                     # First attempt to serve request as file, then
                     # as directory, then fall back to displaying a 404.
                     try_files $uri $uri/ /index.php?$args;
             }

             location ~ \.php$ {
                     include snippets/fastcgi-php.conf;
                     fastcgi_pass unix:/run/php/php7.2-fpm.sock;
                     fastcgi_intercept_errors on;
                     fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
             }
     }



App source
----------

.. literalinclude:: ../../../../_static/source/ws_copiloto.sql
   :language: sql
   :linenos:


Command Source
--------------

.. literalinclude:: ../../../../_static/source/WscopilotoCommand.php
   :language: php
   :linenos:


UML Diagram
===========

  .. graphviz:: ../../../../_static/dot/ws.dot


Relation Zam-Copiloto
=====================

  .. graphviz:: ../../../../_static/dot/relation_tables.dot

---
title: Speech
author: Jesus Baizabal
date: Jul 08, 2023
output: pdf_document
---

# Start

Hello my name is Jesus Mendoza ,  
i am a Software Engineer since 2008 ,  
in the begining start with c++ language in the university,  
my studies was in business administration  
and theoretical physics  
so i'm a self-taught person because  
i became passionate about systems development.

---

My experience in software development  
have been for several years with companies  
and clients from small business  
to corporate enterprises.

---

I main envolved in many types of  
system developments like:  
management of PYMEs,  
management of fuels,  
payment booths conciliations,  
accounting tools ,  
logistics Indicators ,  
development of financial reports ,  
taxi fleet management,  
,positioning software ,  
,building apis for Enterprise Resource Planning Software.

---

My skills are in frontend and backend ,  
but more in the last.  
some languages that i am  
been working with , are:

- php and his frameworks 9 years
- js and libraries like jquery or prototype 9 years
- css html and bootstrap 9 yrs
- python 7 years .i am using python mainly for server  
  and system administration
- Sql language 7 years . Im been using sql a lot
- django 2 year
- react.js 2 years
- angular 2 years
- vue.js 2 years
- api building 4 years
- cloud technologies and containers 4 years

---

One of my successful project  
has been in my current employment ,  
that consist in build  
a web platform for contains all applications  
from the company im working whit  
this platform is easy for upgrade  
and maintainable ,is modular .  
has an easy steps for installation  
an users level and administration module ,  
Today this platform which concentrates  
many of the apps that i mention above  
are up and running in a happy productive state  
since 7 years ago.

---

how i am see in the next 3 years .  
Well i would like develop  
but i like work in something  
relationate to Artificial Intelligence  
or in a ambitious project  
that's it useful  
for many people  
like a opensource project  
for example

---

Why i would like to work in your company ?  
well , i think that i have a lot of experience to share  
and i can deploy my skills in your projects  
of course i think i can professional grow up and  
i believe that you can provide me that environment

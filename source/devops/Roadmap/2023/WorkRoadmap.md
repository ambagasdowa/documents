---
title: "UES Work Roadmap"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 18
  margin:
    top: 1
    bottom: 0
  padding:
    top: 1
    bottom: 1
---

# Roadmap Working Agenda Update 2023

### Projects

- Software Development
  - Logistic [Taxi,Bicycle,Tractos,LastMile,Almacen,Llantas,Operations,Liquidations,etc]
  - Minorist Stores [Almacen , in , outs control, inventory , SAT , ]
  - BookManagement [book creator , exam creator , mood scheme , Jupyter notebook ]
- Virtualizations Services [Infraestructure , kvm , vpn ]
- Development Studio Creation

### Work

- TBK - GST
  - Addenda [Development]
  - Call Recorder [Implementation - Ecosystem]
  - Modelo [Development - Web Service]
  - High Avalibility [Server Configuration - Ecosystem]

### Priority Projects

- Development
  - Payments module [api => visa,paypal,oxxo,mercadopago...]
  - Communication module [api => mail,messages,phone]
- Infraestructure Services
  - Full Virtualization Ecosystem [new hardware adquisition ,supporting iommu ,pci passthrough,gpu isolation]

### Development Roadmap

- Main Language Platform
  - python (Backend-Api)
  - Javascript vanilla ES6 (frontend,canvas,webgl)
  - mysql/postgresql/mssql (Storage Database)

### Documentation

- Python
  - Sphinx

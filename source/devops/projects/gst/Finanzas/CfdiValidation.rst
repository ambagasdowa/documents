********************************************
Componentes para la Validacion de CFDI [SAT]
********************************************

.. .. contents:: Indice


Estructura del Sistema
======================

:Abstrac:

Creacion de un validador de documentos CFDI del SAT y captura de UUID


:Python Example:

  Request Struture :
  url:
      https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl
  Headers:
      Content-type: text/xml;charset="utf-8"
      Accept: text/xml
      SOAPAction: http://tempuri.org/IConsultaCFDIService/Consulta
      cache-control: no-cache
      Host: consultaqr.facturaelectronica.sat.gob.mx
      //accept-encoding: gzip, deflate
      //content-length: 414
      //Connection: close
  Body :
    .. code-block:: xml
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
       <soapenv:Header/>
       <soapenv:Body>
          <tem:Consulta>
              <tem:expresionImpresa><![CDATA[?re=TBO7911166Z6&rr=EUM000707DQ2&tt=12992.00&id=40114629-98fe-4ded-9d4b-5b11b2b4ae43]]></tem:expresionImpresa>
             <!-- <tem:expresionImpresa><![CDATA[?re=LSO1306189R5&rr=GACJ940911ASA&tt=4999.99&id=e7df3047-f8de-425d-b469-37abe5b4dabb]]></tem:expresionImpresa> -->
          </tem:Consulta>
       </soapenv:Body>
      </soapenv:Envelope>
    Response :
    .. code-block:: xml
      <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
          <s:Body>
              <ConsultaResponse xmlns="http://tempuri.org/">
                  <ConsultaResult xmlns:a="http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                      <a:CodigoEstatus>S - Comprobante obtenido satisfactoriamente.</a:CodigoEstatus>
                      <a:EsCancelable>Cancelable con aceptaciÃ³n</a:EsCancelable>
                      <a:Estado>Vigente</a:Estado>
                      <a:EstatusCancelacion/>
                  </ConsultaResult>
              </ConsultaResponse>
          </s:Body>
      </s:Envelope>

  variables que se deben proporcionar para la consulta de ws

  - re={rfc_emisor}
  - rr={rfc_receptor}
  - tt={total}
  - id={uuid}

  :Ejemplo en python:

  .. code-block:: python

    import sys
    from pysimplesoap.client import SoapClient

    data = {
        'rfc_emisor': sys.argv[1],
        'rfc_receptor': sys.argv[2],
        'total': sys.argv[3],
        'uuid': sys.argv[4],
    }
    service = 'https://consultaqr.facturaelectronica.sat.gob.mx/consultacfdiservice.svc?wsdl'
    client = SoapClient(wsdl = service)
    fac = '?re={rfc_emisor}&rr={rfc_receptor}&tt={total}&id={uuid}'.format(**data)
    res = client.Consulta(fac)
    if 'ConsultaResult' in res:
        print ('Estatus: %s' % res['ConsultaResult']['Estado'])
        print ('Cà¸£à¸“digo de Estatus: %s' % res['ConsultaResult']['CodigoEstatus'])


 .. code-block:: php


     // dom specially gives two functions to validate with schema. One is to give file path
     $doc = new DOMDocument();
     $doc->load('PATH TO XML');

     $is_valid_xml = $doc->schemaValidate('PATH TO XSD');

     // or else you could use
     $is_valid_xml = $doc->schemaValidateSource($source)

     // This source should be a string containing the schema. It seems that you are using schemaValidateSource function other than schemaValidate. (Once I was stuck in the same place) cheers


Example from php.net

  This is a working php Example

  .. code-block:: xml

    <!-- example.xml -->
    <?xml version="1.0"?>
    <example>
        <child_string>This is an example.</child_string>
        <child_integer>Error condition.</child_integer>
    </example>

    <!-- example.xsd -->
    <?xml version="1.0"?>
    <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
    elementFormDefault="qualified">
        <xs:element name="example">
            <xs:complexType>
                <xs:sequence>
                    <xs:element name="child_string" type="xs:string"/>
                    <xs:element name="child_integer" type="xs:integer"/>
                </xs:sequence>
            </xs:complexType>
        </xs:element>
    </xs:schema>

.. code-block:: php

      function libxml_display_error($error)
      {
          $return = "<br/>\n";
          switch ($error->level) {
              case LIBXML_ERR_WARNING:
                  $return .= "<b>Warning $error->code</b>: ";
                  break;
              case LIBXML_ERR_ERROR:
                  $return .= "<b>Error $error->code</b>: ";
                  break;
              case LIBXML_ERR_FATAL:
                  $return .= "<b>Fatal Error $error->code</b>: ";
                  break;
          }
          $return .= trim($error->message);
          if ($error->file) {
              $return .=    " in <b>$error->file</b>";
          }
          $return .= " on line <b>$error->line</b>\n";

          return $return;
      }

      function libxml_display_errors() {
          $errors = libxml_get_errors();
          foreach ($errors as $error) {
              print libxml_display_error($error);
          }
          libxml_clear_errors();
      }

      // Enable user error handling
      libxml_use_internal_errors(true);

      $xml = new DOMDocument();
      $xml->load('example.xml');

      if (!$xml->schemaValidate('example.xsd')) {
          print '<b>DOMDocument::schemaValidate() Generated Errors!</b>';
          libxml_display_errors();
      }


:Test Ambient:
SLTestApp
SLTestSys

sysadmin::SLtest09

:Validation Squema:

Validations APDoc Status
------------------------
  ApDoc.Status

-  I = Parcialmente Cancelado
-  V = Cancelado
-  H = Retenido
-  B = Balanceado
-  S = Parcialmente liberado
-  C = Completo
-  U = No asentado
-  P = Asentado

Process in Released
-------------------

  ApDoc.Rlsed = 0
  Ingreso en Almacen
  APDoc.status = H

  ApDoc.Rlsed = 1
  Validacion en SAT
  APDoc.status = U

  ApDoc.Rlsed = 1
  Liberacion en SL
  APDoc.status = P


Validation Squema
-----------------
  For validation go to H status to U
  -- Table from Almacen build in set update H to U
  After validation , insert into tables

  integraapp.dbo.GRW_CfdiInvcAddData
  integraapp.dbo.xCEEIDoc

and in test in
  sltestapp.dbo.GRW_CfdiInvcAddData
  sltestapp.dbo.xCEEIDoc

insert into uuid table

    -- tr after insert into xml
      --- build date() and update "programacion para pago " in APDoc
          [validation.date + dias de credito ] + next Wednesday
          [Calcualte the Period with this date]
          {
            update
              apdoc.DueDate	 = new_date()
              apdoc.PayDate	 = new_date()
              apdoc.PerPost  = PerPost(new_date())
              apdoc.User1    = 'EC'
          }
      --- insert BatNbr into wkreleased table
      --- exec proc {		Exec pp_03400 'SQLEC', 'SQLEC'  }
      --- delete BatNbr from wkreleased
      --- delete BatNbr from wkreleasedbad
      --- insert BatNbr into wkreleased
      
App source
----------

.. literalinclude:: ../../../../_static/source/ws_copiloto.sql
   :language: sql
   :linenos:


Command Source
--------------

.. literalinclude:: ../../../../_static/source/WscopilotoCommand.php
   :language: php
   :linenos:


UML Diagram
===========

  .. graphviz:: ../../../../_static/dot/ws.dot


Relation Zam-Copiloto
=====================

  .. graphviz:: ../../../../_static/dot/relation_tables.dot

Resources
=========

sw:sapien
  https://developers.sw.com.mx/knowledge-base/servicio-publico-de-consulta-estatus-cfdi-sat/

cfdiUtils
  https://cfdiutils.readthedocs.io/es/latest/componentes/estado-sat/

  https://github.com/phpcfdi/sat-estado-cfdi-soap

SAT
  https://verificacfdi.facturaelectronica.sat.gob.mx/

********************************
Components of Units Avaliability
********************************

Programation Paradigm : MVC 
===========================


:Historical Method:


======================================================= ========================= ===============
TableName                                               function                  Order in UX
======================================================= ========================= ===============
disponibilidad_currentview_rpt_unidades_gst_indicators  currentTimeView           -
disponibilidad_tbl_hist_rpt_unidades_gst_indicators     historicalView            -
disponibilidad_main_view_rpt_unidades_gst_indicators    [current+historical]view  3rd_table_Detail
======================================================= ========================= ===============


:UIX relations:

======================================================= ================== ============================================ ===========================
TableName                                               UX element         Variable                                     Order in UX
======================================================= ================== ============================================ ===========================
DisponibilidadViewCrossName                             table_one          $dispCross                                   1st table percents
DisponibilidadViewRptGroupClassificationsIndicator      table_2nd_section  $disponibilidadViewClassifications           2nd table indicator resume
DisponibilidadMainViewRptUnidadesGstIndicators          table_three        $DisponibilidadViewRptUnidadesGstIndicators  3rd table Detail
======================================================= ================== ============================================ ===========================

:TODO:

================================== ============================================================ =======
Requirement                        Description                                                  Status
================================== ============================================================ =======
Expand days graphics (3)           Add historical Daily support                                 [X]
Fix width in table one (4)         make the width in table automatic                            [ ]
Check integer values(4)            print only interger values normal round                      [ ]
minor checking operations(6)       sum and promedium revisions                                  [ ]
================================== ============================================================ =======



.. raw:: html

    <object data="../../../../_static/dot/ws.svg" type="image/svg+xml"></object>



.. raw:: html
   
      <div class="mermaid">
      flowchart TD
          Start --> Stop
      </div>
      

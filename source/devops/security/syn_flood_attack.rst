***************************************************
Syn Flood Attack [Ataque de denegación de servicio]
***************************************************

.. .. contents:: Indice


De Wikipedia, la enciclopedia libre

Schema-Stachledraht de un Ataque DDos
-------------------------------------

   .. image:: https://upload.wikimedia.org/wikipedia/commons/3/3f/Stachledraht_DDos_Attack.svg
      :alt: Schema-Stachledraht
      :align: center

Diagrama de un ataque DDoS usando el software Stacheldraht.

  En `seguridad informática <https://es.wikipedia.org/wiki/Seguridad_inform%C3%A1tica%20%22Seguridad%20informática%22>`__,
  un **ataque de denegación de servicio**, también llamado ataque **DoS** (por sus siglas en inglés, ***D**\ enial **o**\ f **S**\ ervice*), es un
  ataque a un sistema de `computadoras <https://es.wikipedia.org/wiki/Computadora>`__ o
  [red](https://es.wikipedia.org/wiki/Red\_de\_computadoras "Red de computadoras" ) que causa que un servicio o recurso sea inaccesible a
  los usuarios legítimos.[1]​ Normalmente provoca la pérdida de la conectividad con la red por el consumo del `ancho de
  banda <https://es.wikipedia.org/wiki/Ancho_de_banda>`__ de la red de la víctima o sobrecarga de los recursos computacionales del sistema
  atacado. Un ejemplo notable de ello se produjo el `27 de marzo <https://es.wikipedia.org/wiki/27_de_marzo>`__ de
  `2013 <https://es.wikipedia.org/wiki/2013>`__, cuando el ataque de una empresa a otra inundó la
  [red](https://es.wikipedia.org/wiki/Red\_de\_computadoras "Red de computadoras" ) de `correos
  basura <https://es.wikipedia.org/wiki/Spam>`__ provocando una ralentización general de
  `Internet <https://es.wikipedia.org/wiki/Internet%20%22Internet%22>`__ e incluso llegó a afectar a puntos clave como el `nodo
  central <https://es.wikipedia.org/wiki/Punto_neutro>`__ de `Londres <https://es.wikipedia.org/wiki/Londres>`__.[2]​


  Los ataques DoS se generan mediante la saturación de los puertos con
  múltiples flujos de información, haciendo que el
  `servidor <https://es.wikipedia.org/wiki/Servidor>`__ se sobrecargue y
  no pueda seguir prestando su servicio. Por eso se le denomina
  *denegación*, pues hace que el servidor no pueda atender la cantidad
  enorme de solicitudes. Esta técnica es usada por los
  `*crackers* <https://es.wikipedia.org/wiki/Cracker>`__ o piratas
  informáticos para dejar fuera de servicio servidores objetivo. A nivel
  global, este problema ha ido creciendo, en parte por la mayor facilidad
  para crear ataques y también por la mayor cantidad de equipos
  disponibles mal configurados o con fallos de seguridad que son
  explotados para generar estos ataques. Se ve un aumento en los ataques
  por reflexión y de amplificación por sobre el uso de
  `botnets <https://es.wikipedia.org/wiki/Botnet>`__.[3]​

  Una ampliación del ataque DoS es el llamado **ataque de denegación de
  servicio distribuido**, también llamado **DDoS** (por sus siglas en
  inglés, ***D**\ istributed **D**\ enial **o**\ f **S**\ ervice*) el cual
  se lleva a cabo generando un gran flujo de información desde varios
  puntos de conexión hacia un mismo punto de destino. La forma más común
  de realizar un DDoS es a través de una `red de
  bots <https://es.wikipedia.org/wiki/Botnet>`__, siendo esta técnica el
  `ciberataque <https://es.wikipedia.org/wiki/Ciberataque%20%22Ciberataque%22>`__
  más usual y eficaz por su sencillez tecnológica.

  En ocasiones, esta herramienta ha sido utilizada como un buen método
  para comprobar la capacidad de tráfico que un ordenador puede soportar
  sin volverse inestable y afectar los servicios que presta. Un
  administrador de redes puede así conocer la capacidad real de cada
  máquina.

Métodos de ataque
-----------------

  Un ataque de denegación de servicio impide el uso legítimo de los
  usuarios al usar un servicio de red. El ataque se puede dar de muchas
  formas. Pero todas tienen algo en común: utilizan la familia de
  protocolos `TCP/IP <https://es.wikipedia.org/wiki/TCP/IP>`__ para
  conseguir su propósito.

  Un ataque DoS puede ser perpetrado de varias formas. Aunque básicamente
  consisten en:

  -  Consumo de recursos computacionales, tales como ancho de banda,
     espacio de disco, o tiempo de procesador.
  -  Alteración de información de configuración, tales como información de
     rutas de encaminamiento.
  -  Alteración de información de estado, tales como interrupción de
     sesiones TCP (TCP reset).
  -  Interrupción de componentes físicos de red.
  -  Obstrucción de medios de comunicación entre usuarios de un servicio y
     la víctima, de manera que ya no puedan comunicarse adecuadamente.

Inundación SYN (SYN Flood)
~~~~~~~~~~~~~~~~~~~~~~~~~~

Principios de TCP/IP
^^^^^^^^^^^^^^^^^^^^

  Cuando una máquina se comunica mediante
  `TCP/IP <https://es.wikipedia.org/wiki/TCP/IP>`__ con otra, envía una
  serie de datos junto a la petición real. Estos datos forman la cabecera
  de la solicitud. Dentro de la cabecera se encuentran unas señalizaciones
  llamadas *Flags* (`banderas <https://es.wikipedia.org/wiki/Flags>`__).
  Estas señalizaciones (banderas) permiten iniciar una conexión, cerrarla,
  indicar que una solicitud es urgente, reiniciar una conexión, etc. Las
  banderas se incluyen tanto en la solicitud (cliente), como en la
  respuesta (servidor).

  Para aclararlo, veamos cómo es un intercambio estándar TCP/IP:

  1) Establecer Conexión: el cliente envía una Flag SYN; si el servidor
     acepta la conexión, éste debería responderle con un SYN/ACK; luego el
     cliente debería responder con una Flag ACK.
     
     
  .. code-block:: bash
    
     1-Cliente --------SYN-----> 2 Servidor 4-Cliente <-----SYN/ACK---- 3
     Servidor 5-Cliente --------ACK-----> 6 Servidor

  2) Resetear Conexión: al haber algún error o pérdida de paquetes de
     envío se establece envío de Flags RST:

  .. code-block:: bash

     1-Cliente -------Reset-----> 2-servidor 4-Cliente <----Reset/ACK----
     3-Servidor 5-Cliente --------ACK------> 6-Servidor

  La inundación SYN envía un flujo de paquetes TCP/SYN (varias peticiones
  con Flags SYN en la cabecera), muchas veces con la dirección de origen
  falsificada. Cada uno de los paquetes recibidos es tratado por el
  destino como una petición de conexión, causando que el servidor intente
  establecer una conexión al responder con un paquete TCP/SYN-ACK y
  esperando el paquete de respuesta TCP/ACK (Parte del proceso de
  establecimiento de conexión TCP de 3 vías). Sin embargo, debido a que la
  dirección de origen es falsa o la dirección IP real no ha solicitado la
  conexión, nunca llega la respuesta.

  Estos intentos de conexión consumen recursos en el servidor y copan el
  número de conexiones que se pueden establecer, reduciendo la
  disponibilidad del servidor para responder peticiones legítimas de
  conexión.

  `SYN cookies <https://es.wikipedia.org/wiki/SYN_cookies>`__ provee un
  mecanismo de protección contra Inundación SYN, eliminando la reserva de
  recursos en el host destino, para una conexión en momento de su gestión
  inicial.

Inundación ICMP (ICMP Flood)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~


  Es una técnica DoS que pretende agotar el ancho de banda de la víctima.
  Consiste en enviar de forma continuada un número elevado de paquetes
  *`ICMP <https://es.wikipedia.org/wiki/ICMP>`__* *Echo request*
  (`ping <https://es.wikipedia.org/wiki/Ping>`__) de tamaño considerable a
  la víctima, de forma que esta ha de responder con paquetes
  *`ICMP <https://es.wikipedia.org/wiki/ICMP>`__* *Echo reply*
  (`pong <https://es.wikipedia.org/wiki/Pong>`__) lo que supone una
  sobrecarga tanto en la red como en el sistema de la víctima.

  Dependiendo de la relación entre capacidad de procesamiento de la
  víctima y el atacante, el grado de sobrecarga varía, es decir, si un
  atacante tiene una capacidad mucho mayor, la víctima no puede manejar el
  tráfico generado.

SACKPanic
---------
  
  Manipulando el `Tamaño Máximo de
  Segmento <https://es.wikipedia.org/wiki/Tama%C3%B1o_M%C3%A1ximo_de_Segmento%20%22Tamaño%20Máximo%20de%20Segmento%22>`__
  y la `Retransmisión (redes de
  datos) <https://es.wikipedia.org/w/index.php?title=Retransmisi%C3%B3n_(redes_de_datos)&action=edit&redlink=1%20%22Retransmisión%20(redes%20de%20datos)%20(aún%20no%20redactado)%22>`__,
  en `inglés <https://es.wikipedia.org/wiki/Idioma_ingl%C3%A9s>`__,
  *selective acknowledgement* o SACK, pueden ser usados de manera remota
  para causar una ataque de de denegación de servicios por medio de un
  desborde de entero en el kernel de Linux,[4]​ incluso causando un
  `Kernel panic <https://es.wikipedia.org/wiki/Kernel_panic>`__.[5]​
  Jonathan Looney descubrió las CVE-2019-11477, CVE-2019-11478,
  CVE-2019-11479 el 17 de junio de 2019.[6]​

SMURF
-----

  Artículo principal: *`Ataque
  smurf <https://es.wikipedia.org/wiki/Ataque_smurf>`__*

  Existe una variante a *ICMP Flood* denominado Ataque Smurf que amplifica
  considerablemente los efectos de un ataque
  `ICMP <https://es.wikipedia.org/wiki/ICMP>`__.

  Existen tres partes en un Ataque Smurf: El atacante, el intermediario y
  la víctima (comprobaremos que el intermediario también puede ser
  víctima).

  En el ataque Smurf, el atacante dirige paquetes
  `ICMP <https://es.wikipedia.org/wiki/ICMP>`__ tipo "*echo request*"
  (ping) a una dirección IP de broadcast, usando como dirección IP origen,
  la dirección de la víctima
  (`Spoofing <https://es.wikipedia.org/wiki/IP_spoofing%20%22IP%20spoofing%22>`__).
  Se espera que los equipos conectados respondan a la petición, usando
  *Echo reply*, a la máquina origen (víctima).

  Se dice que el efecto es amplificado, debido a que la cantidad de
  respuestas obtenidas, corresponde a la cantidad de equipos en la red que
  puedan responder. Todas estas respuestas son dirigidas a la víctima
  intentando colapsar sus recursos de red.

  Como se dijo anteriormente, los intermediarios también sufren los mismos
  problemas que las propias víctimas.

Inundación UDP (UDP Flood)
~~~~~~~~~~~~~~~~~~~~~~~~~~

  Básicamente este ataque consiste en generar grandes cantidades de
  paquetes `UDP <https://es.wikipedia.org/wiki/UDP>`__ contra la víctima
  elegida. Debido a la naturaleza sin conexión del protocolo UDP, este
  tipo de ataques suele venir acompañado de `IP
  spoofing <https://es.wikipedia.org/wiki/IP_spoofing>`__.

  Es usual dirigir este ataque contra máquinas que ejecutan el servicio
  Echo, de forma que se generan mensajes Echo de un elevado tamaño.
  
How To Mitigate the Attack
~~~~~~~~~~~~~~~~~~~~~~~~~~

  SYN flood vulnerability has been known for a long time and a number of mitigation pathways have been utilized. A few approaches include:
  Increasing Backlog queue

  Each operating system on a targeted device has a certain number of half-open connections that it will allow. One response to high volumes of SYN packets is to increase the maximum number of possible half-open connections the operating system will allow. In order to successfully increase the maximum backlog, the system must reserve additional memory resources to deal with all the new requests. If the system does not have enough memory to be able to handle the increased backlog queue size, system performance will be negatively impacted, but that still may be better than denial-of-service.
  Recycling the Oldest Half-Open TCP connection

  Another mitigation strategy involves overwriting the oldest half-open connection once the backlog has been filled. This strategy requires that the legitimate connections can be fully established in less time than the backlog can be filled with malicious SYN packets. This particular defense fails when the attack volume is increased, or if the backlog size is too small to be practical.
  SYN cookies

  This strategy involves the creation of a cookie by the server. In order to avoid the risk of dropping connections when the backlog has been filled, the server responds to each connection request with a SYN-ACK packet but then drops the SYN request from the backlog, removing the request from memory and leaving the port open and ready to make a new connection. If the connection is a legitimate request, and a final ACK packet is sent from the client machine back to the server, the server will then reconstruct (with some limitations) the SYN backlog queue entry. While this mitigation effort does lose some information about the TCP connection, it is better than allowing denial-of-service to occur to legitimate users as a result of an attack.

Defending SYN Flood Attack
~~~~~~~~~~~~~~~~~~~~~~~~~~

  • Using SYN cookies

  This is the most effective method of defending from SYN Flood attack. The use of SYN cookies allow a server to avoid dropping connections when the SYN queue fills up. Instead, the server behaves as if the SYN queue has been enlarged. The server sends back the appropriate SYN+ACK response to the client but discards the SYN queue entry. If the server then receives a subsequent ACK response from the client, it is able to reconstruct the SYN queue entry using information encoded in the TCP sequence number.

  SYN cookies can be enabled by adding the following to /etc/sysctl.conf

      net.ipv4.tcp_syncookies = 1

  After modifying the sysctl configuration file, you need to execute the following command to load sysctl settings from the file /etc/sysctl.conf

      sysctl –p

  • Increasing the SYN backlog queue

  An optional defending technique is to increase the SYS backlog queue size. The default size is 1024. This can be done by adding the following to /etc/sysctl.conf

      net.ipv4.tcp_max_syn_backlog = 2048

  • Reducing SYN_ACK retries

  Tweaking the kernel parameter tcp_synack_retries causes the kernel to close the SYN_RECV state connections earlier. Default value is 5.

      net.ipv4.tcp_synack_retries = 3

  • Setting SYN_RECV timeout

  Lowering the timeout value for SYN_RECV will help in reducing the SYN flood attack. The default value is 60 and we can reduce it to 40 or 45. This can be done by adding the following line to sysctl.conf.

      net.ipv4.netfilter.ip_conntrack_tcp_timeout_syn_recv=45

  • Preventing IP spoofing

  The following sysctl parameter will help to protect against IP spoofing which is used for SYN flood attacks.

      net.ipv4.conf.all.rp_filter = 1

  
GST Cases :
===========


Ataque al Servidor Terminal 1 [Jul-2019] 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  
  En Julio de 2019 se detectaron caidas del servicio RDP causando tiempos de espera para poder ingresas y utilizar el servicio
  al analizar los puertos utilizados se advierte que existen peticiones sin responder por parte de algunas ips
   
   
=============== ================= =============== ==============   ===========
Ataque          Servidor          ip-victim       ip-attacker
=============== ================= =============== ==============   ===========
SYN-FLOOD       IntegraT1         192.168.20.237  202.230.138.68
SYN-FLOOD       IntegraT1         192.168.20.237  23.252.70.20
SYN-FLOOD       IntegraT1         192.168.20.237  18.182.190.208
SYN-FLOOD       IntegraT1         192.168.20.237  101.79.0.58
=============== ================= =============== ==============   ===========

Al revisar los puertos de RDP se encuentran ip con el status SYN_ACKNOWLEDGMENT 

.. code-block:: bat

   netstat -ona | find "SYN_" 

.. image:: ../../_static/gst/security/syn_flood.png


.. code-block:: bat

   netstat -ona | find "3390" 
  
.. image:: ../../_static/gst/security/SYN_flood_attack.png


Monitoreando el puerto 3390 que usa el Servidor muestra caidas del servicio 

.. image:: ../../_static/gst/security/scann.png

Como Mitigar El ataque en Windows 2008 R2

  :metodo 1:
  bloquear mediante firewall las ips 
  
  .. image:: ../../_static/gst/security/firewall_block.png
  
  :metodo 2:
  mitigar el ataque a cualquier ip 
  
  .. image:: ../../_static/gst/security/add_reg_keys.png
  
  
   unefon - 01 800 333 00 50.

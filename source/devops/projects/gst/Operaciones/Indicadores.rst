*****************************************
Componentes de los Indicadores Operativos
*****************************************


Definicion de Conceptos
=======================

:FlagIsProvision:
  La Provision , aplica en Indicador Despachado Consiste en las Cartas Portes de un Viaje Despachado que aun se encuentran en un status de Edicion, en el indicador Aceptado es correcto que sea 0 y en el indicador "Despachado" aparecen 0 y 1

:FlagIsDisminution:
  Cartas Porte Canceladas , son las cartas porte que se cancelaron en el mes consultado y se aceptaron en meses diferentes , el monto se mantiene vigente en el mes consultado y se resta en el siguiente mes. su identificador es 1 , este concepto no aplica en el indicador Excel

:FlagIsNextMonth:
  Indica que son cartas porte canceladas que provienen del mes anterior[FlagIsDisminution] y se restan del mes consultado. su identificador es 1 , este concepto no aplica en el indicador Excel


Portal Web
==========
  El indicador Aceptado en el portal web se compone de :
  Aceptado + Canceladas y aceptadas en meses diferentes del mes consultado + Disminucion de Canceladas mes Anterior

  Ejemplo Mayo 2019 - Guadalajara - Encortinados
  Aceptado Web se compone de la suma

  .. image:: ../../../../_static/gst/acpt_web_t.png

  El monto de cancelaciones debera aparecer en el siguiente mes Junio disminuyendo el Ingreso

  .. image:: ../../../../_static/gst/monto_disminucion.png
  .. image:: ../../../../_static/gst/monto_sum_dism.png
  .. image:: ../../../../_static/gst/disminucion_web.png

=============== ================= =============== ============ ===========
FlagIsProvision FlagIsDisminution FlagIsNextMonth              Descripcion
=============== ================= =============== ============ ===========
0               0                 0               Aceptado Web Aceptado
0               1                 0               Aceptado Web Cancelados y aceptados en meses diferentes
0               0                 1               Aceptado Web Disminucion de canceladas provenientes del mes anterior
=============== ================= =============== ============ ===========

  .. image:: ../../../../_static/gst/map_aceptado.png
  .. image:: ../../../../_static/gst/may_ing_gdl_zoom.png


Portal Excel
============
El indicador Aceptado en el xls se compone de :
Aceptado unicamente

Ejemplo Mayo 2019 - Guadalajara - Encortinados
Aceptado se compone de la suma

  .. image:: ../../../../_static/gst/aceptado_xls.png


=============== ================= =============== ============ ===========
FlagIsProvision FlagIsDisminution FlagIsNextMonth              Descripcion
=============== ================= =============== ============ ===========
0               0                 0               Aceptado Xls Aceptado
=============== ================= =============== ============ ===========

- Aplicando el filtro para encortinados , el ejercicio es valido agrupando el tipo carga "OTROS"

  .. image:: ../../../../_static/gst/filtro_op.png
  .. image:: ../../../../_static/gst/xls_encortinado.png


Indicador Despacho
==================
Portal Web
El indicador Despachado en el portal web se compone de :
Aceptado + Aceptado meses diferentes Documento + Provision[Documento en Edicion] + Canceladas y aceptadas en meses diferentes del mes consultado + Disminucion de Canceladas mes Anterior

La fecha que rige esta consulta es distinta del Documento

  .. image:: ../../../../_static/gst/despacho_web.png

=============== ================= =============== ============== ===========
FlagIsProvision FlagIsDisminution FlagIsNextMonth                Descripcion
=============== ================= =============== ============== ===========
0               0                 0               Despachado Web Aceptado
0               0                 0               Despachado Web Aceptado meses Distinto al Documento CP
1               0                 0               Despachado Web Provision Documento CP en Edicion
0               1                 0               Despachado Web Cancelados y aceptados en meses diferentes
0               0                 1               Despachado Web Disminucion de canceladas provenientes del mes anterior
=============== ================= =============== ============== ===========

Indicador Excel
===============

=============== ================= =============== ============== ===========
FlagIsProvision FlagIsDisminution FlagIsNextMonth                Descripcion
=============== ================= =============== ============== ===========
0               0                 0               Despachado xls Aceptado
0               0                 0               Despachado xls Aceptado meses Distinto al Documento CP
1               0                 0               Despachado xls Provision Documento CP en Edicion
=============== ================= =============== ============== ===========

 	fe80d750ee413b07b769662078fd09a5

Port de los archivos excel al cloud
===================================

 Version Web de los archivos excel [Indicadores Operativos]

 :Definicion de Formulas:

 - Tendencias en Secciones REAL ,Presupuesto y Desviacion.

============= ==================================  ====================================================  ===========================
Section       REAL                                Presupuesto                                           Desviacion
============= ==================================  ====================================================  ===========================
Days
              DiasLab                             DiasTrns
Despachado
VOLUMEN       TnsTtDsp                            h=({[TnsTtDsp/DiasTrns]DiasLab}/DiasLab)DiasTrns      (TnsTtDsp-h) && (TnsTtDsp-h)/PptoTns
KILOMETROS    KmsTtDsp                            i=({[KmsTtDsp/DiasTrns]DiasLab}/DiasLab)DiasTrns      (KmsTtDsp-i) && (KmsTtDsp-i)/PptoKms
VIAJES        VjsTtDsp                            j=({[VjsTtDsp/DiasTrns]DiasLab}/DiasLab)DiasTrns      (VjsTtDsp-j) && (VjsTtDsp-j)/PptoVjs
INGRESOS      IngsTtDsp                           k=({[IngsTtDsp/DiasTrns]DiasLab}/DiasLab)DiasTrns     (IngsTtDsp-k) && (IngsTtDsp-k)/PptIngs
B
VOLUMEN       (TnsTtDsp/DiasTrns)DiasLab          PptoTns                                               (TnsTtDsp-PptoTns) && (TnsTtDsp-PptoTns)/PptoTns
KILOMETROS    (KmsTtDsp/DiasTrns)DiasLab          PptoKms                                               (KmsTtDsp-PptoKms) && (KmsTtDsp-Pptokms)/PptoKms
VIAJES        (VjsTtDsp/DiasTrns)DiasLab          PptoVjs                                               (VjsTtDsp-PptoVjs) && (VjsTtDsp-PptoVjs)/PptoVjs
INGRESOS      (IngsTtDsp/DiasTrns)DiasLab         PptIngs                                               (IngsTtDsp-PptIngs)&&(IngsTtDsp-PptIngs)/PptIngs
Aceptado
VOLUMEN       TonsTtAcp
KILOMETROS    KmsTtAcp
VIAJES        VjsTnAcp
X
KMS X VIAJE   a=KmsTtDsp/VjsTtDsp                 b=i/j                                                 c=a-b && [c/b]%
TON X VIAJE   d=TnsTtDsp/VjsTtDsp                 e=h/j                                                 f=d-e && [f/e]%
Y
CARGA DIARIA  l=TnsTtDsp/DiasTrns                 m=h-DiasTrns                                          n=m-l && [n/m]%
Z
UNITS         o=granel?(l/d)/0.7:(l/d)/0.5;       p=granel?(m/e)/0.772:(m/e)/0.21;                      q=p-o && [q/p]%
============= ==================================  ====================================================  ===========================
